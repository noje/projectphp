<div class="contenu">
<code>                                                                                                        
                        */,                         
                                                            
                                                            
                      ((     /(#%                           
                     @ %@   @@  (@.                         
                     @  @   *@   @%                         
                     &%%%&&&%(&&                          
                     (##%%%&&%%%(#(                         
                     *##%%%&&&/##(   *                      
                      %/(#####(#%%%  .(,                    
                     @%%%###%%%&@@@@                        
                    @@@&%%%&@@@@@@@@@                       
                   @@@@@@@@@@@@@@@@@@&                      
                  @@@@@@@@@@@@@@@@@@&@                      
                  &@@@@@@@@@@@@@@@@&%&@   .                 
              *  @@@@@@@@@@@@@@@@@@@@@&&/   ,               
             *  @@@@@@@@@@@@@@@@@@@@@@@@@ ** *              
            ,  @@@@@@@@@@@@@@@@@@@@@@@@@@@    .             
              @@@@@@@@@@@@@@@@@@@@@@@@@@@@    ,             
              @@@@@@@@@@@@@@@@@@@@@@@@@@@@                  
           *  @@@@@@@@@@@@@@@@@@@@@@@@@@@@   ,              
         *#,..@@@@@@@@@@@@@@@@@@@@@@@@@@@@ .   .*,          
        #%%%%/ &@@@@@@@@@@@@@@@@@@@@@@@#%%      ..(         
  /((((##%%%%%#   @@@@@@@@@@@@@@@@@@@@@(%%.      %%#        
 (#%%%%%%%%%%%%#    @@@@@@@@@@@@@@@@&%%(%%#(//((#%%%/       
  #%%%%%%%%%%%%%%     @@@@@@@@@@@@@@@&%(%%%####%%%%%%#      
  (%%%%%%%%%%%%%%%/  ,@@@@@@@@@@@@@@@ ,(%%%%%%%%%%%%%%%#(   
 *#%%%%%%%%%%%%%%%#(@@@@@@@@@@@@@@@   *(%%%%%%%%%%%%%%#(/   
 (#%%%%%%%%%%%%%%%%#/,,@@@@@@@@*      /(%%%%%%%%##((///     
 /(((####%%%%%%%%%%#(*                *(#%%%%##(//*         
      ////(((######(/,                ,*/((((//*            
             ***//*,,                 .,,,***,              
                                                            
</code>
<p>
    LINUX est le systeme d'exploitation libre (pour la plupart des distros), au meme titre que Mac os ou windows,
il fait la liaison entre la machine et l'utilisateur.
c'est un systeme trés varié, le nombre de distrib conséquent en est un exemple probant, 
certains systeme ont juste une fonction d'autre sont aussi complet si ce n'est plus 
que ses concurents de chez microsoft ou apple. 

<code>                                                                               
                                                                               
      @@@@@@@@@.                                                               
   @@@@@@@@@@@@@@@             ,,                                              
 @@@  @@@ @@@  @@@&       #@@@@@@@&  @@@  ,,.   @@@    @@@           @@@      
@@@@           @@@@%     %@@@         @   @@@   @@@    @@@           @@@      
%@@@             @@@@     @@@  *%%%%  @@@&@@@@@@ @@@@@@@@@@ %@@   @@@ @@@@@@@@ 
%@@@             @@@@     @@@  &@@@@  @@@  @@@   @@@@@@@@@@ @@@   @@@ @@@   @@%
 @@@@           @@@@@     &@@@   &@@  @@@  @@@   @@@    @@@ %@@   @@@ @@@   @@%
 @@@@@@@    @@@@@@@        @@@@@@@@  @@@  @@@@@ @@@    @@@ @@@@@@@@@ @@@@@@@@ 
  @@        @@@@@                                                             
   @@@     @@@                                                               
                                                                                    
</code>
<p>
GITHUB
&#47;Crée par la meme personne qui nous a offert, c'est un outil web
qui permet l'herbergement et le versionnage d'app, site... cette outil est puissant.
grace à lui j'ai pus mettre mon site à jours en qlq ligne de commande : 
[git push .]
le recuperer dans mon fichier local (voir plus bas)
[git pull .]
j'ai pu suivre l'evolution du site, commenter (avec la commande commit) mes versions de facon sur et rapide.
</p>
<code >        
            .,@@&,                                                 
        *(@@@@@@@@@,.                                             
    ,@@@@@@@@@@@@@@@@*,                                          
    (@@@@@@@@@@@@@@@@@@@@@,                                        
    @@@@@@@@@@@@@@@@@@#,,,,   ,,,,,,,,     ,,,    ,,,,,,,  ,,    ,,
    @@@@@@@@@@@@@@@*,,,,,,,   ,,,   ,,    ,,,,    ,,    ,, ,,    ,,
    @@@@@@@@@@@@,,,,,,,,,,,   ,,,,,,,,   ,,,,,,   ,,,,,,   ,,,,,,,,
    @@@@@@@@@@@,,,#,,,,,,,,   ,,,   ,,,  ,,,,,,,       ,,, ,,    ,,
    @@@@@@@@@@@,,@,,,,,,,,,   ,,,  .,,, ,,    ,, ,,,   ,,, ,,    ,,
    @@@@@@@@@@@,,*&@,,,,,,,   ,,,,,,   ,,,    ,,,  ,,,,,   ,,    ,,
    /(@@@@@@@@@,,@@,,(,,,,    ... .  . . ..   .    .  .        .  .
        .,@@@@@@,,,,,,,,                                            
            ,&@@,,,,(                                               
</code>
<p>
    BASH 
    &#47;ou plus communement appelé la console ou terminal permet de s'affranchir de l'interface graphique pour effectuer
    plus ou moins la meme chose mais de facon plus rapide et pratique mais demande un temps de pratique. Mais une fois accoutumé il en est presque primordial.
    avec le BASH nous pouvons utiliser le gestionnaire de paquets, (commande APT) qui permet de telecharger,
    mettre à jours systeme et application. </br>

    la commande est :</br>
            NomUtilisatteur@NomMachine:~ apt get install vim</br>
    
    &#47;permet d'installer un editeur de texte hexadecimal (pour creer des petits script par exemple).</br>
    Nous avons aussi appris à paramettrer un serveur en local avec
</p>
<code>                                                  
                                            *,           
                                           %%%.        
                                          .% ,%%      
                                          .%    %#    
                                        ,#%%  */  %%   
                                        %%    .////  (%  
                                         %#  /////////  %/ 
                                               *///////, %% 
%%%        #%%%%%%%%(  *//////////*   %%%%%%%%%. ,//////  %# 
%%%        #%%     %%% *//  ///  ///  %%%    %%% ./////*  %  
%%%        #%%     %%% *//  ///  ///  %%%%%%%%%# .////  ,%   
%%%        #%%     %%% *//  ///  ///  %%%             #%(    
#%%%%%%%%% #%%     %%% *//  ///  ///  %%%        .%%%/       
                                    
                                    
</code>
<p>
    LAMP   
    &#47;cette Outil au nom quelque peu etrange est l'accronyme pour Liunx Appache Mysql PHP 
    (de base mais differentes version en fonction d' OS) et le contenu peut varié aussi fonction des besoins et utilisations.
    grace a ce bundle nous avons pu tester notre site comme si il etait en ligne. car un site n'est pas simplement
    un ensemple de dossier et fichier c'est aussi des requetes qui voyage sur l'Internet.
</p>
</div>