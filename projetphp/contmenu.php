<div class="contenu">
    <h1>&lt;&#47;Menu Pâtisseries&gt;</h1>
    <p> &#47;Affichage d'un ficchier XML recuperé via un lien web, puis transformé en tableau 
        et affiché dans le contenu de ce site.</p>

    <?php displayXML(); ?>
</div>


<?php 
    function displayXML(){
        // On récupère le fichier en local
        $xml = simplexml_load_file('https://www.w3schools.com/xml/simple.xml') 
        or die("Erreur: Impossible d'afficher cet objet");
        
        echo '<table classe="tableau">';
        //echo '<th>name</th>';
        // echo '<th>price</th>';
        // echo '<th>descript</th>';
        // echo '<th>Kcal</th>';
        $food = $xml->food;
        foreach($food as $first){
            echo '<tr>';
            
            foreach ($first as $key => $value) {
                echo '<th>';
                    echo $key;
                echo '</th>';
            }
            echo '</tr>';
            break;
        }
        
        foreach($food as $element){
            echo '<tr><td>';
            echo $element->name;
            echo '</td><td>';
            echo $element->price;
            echo '</td><td>';
            echo $element->description;
            echo '</td><td>';
            echo $element->calories;
            echo '</td></tr>';
        }
        
        echo '</table>';
    }
?>
