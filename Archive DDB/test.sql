-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Lun 13 Janvier 2020 à 09:16
-- Version du serveur :  5.7.28-0ubuntu0.18.04.4
-- Version de PHP :  7.2.24-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `test`
--
CREATE DATABASE IF NOT EXISTS `test` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `test`;

-- --------------------------------------------------------

--
-- Structure de la table `blog`
--

DROP TABLE IF EXISTS `blog`;
CREATE TABLE IF NOT EXISTS `blog` (
  `titre` varchar(200) DEFAULT NULL,
  `note` varchar(10000) DEFAULT NULL,
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `blog`
--

INSERT INTO `blog` (`titre`, `note`, `ID`) VALUES
('vbnvbn', '            bvnvbnbv', 54),
('fdsgdf', '            fdgfdg', 56),
('hello ', 'world            ', 58),
('hello ', 'world            ', 59),
('hello ', 'world            ', 60),
('fgdgfd', '            fdgfdg', 61),
('fgdgfd', '            fdgfdg', 62),
('hello MEC', '            regarde', 63),
('hello MEC', '            regarde', 64),
('hello MEC', '            regarde', 66),
('titre', 'note            ', 76),
('test', 'test            ', 77),
('g', '\r\nk\r\n           ', 78);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Login` varchar(100) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `Login`, `email`, `password`) VALUES
(1, 'admin', 'pierre@nn.com', 'admin'),
(2, 'csqdf', 'sfdg@sdgf.fr', '123'),
(3, 'csqdf', 'sfdg@sdgf.fr', '123'),
(4, 'hello', 'hello@mail.fr', '1234'),
(5, 'er', 'er@mail.fr', '1234'),
(6, 'admin', 'admin@mail.com', 'admin1'),
(7, 'zer', 'zer@mail.fr', '1234'),
(8, 'zer', 'zer@mail.fr', '1234'),
(9, 'noje', 'noje@mail.fr', '123'),
(10, 'remi', 'tonfion.salenazijuif@gmail.com', 'remi'),
(11, 'remi', 'cecinestpasunvraimail@mailHazard.com', 'remi');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
