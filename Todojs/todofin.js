let item1 = {
    texte :"remplir ma liste",
    completed : false
}
let item2 = {
    texte :"effacer ma liste",
    completed : true
}
let item3 = {
    texte :"modifier ma liste",
    completed : false
}
let item4 = {
    texte :"faire ma liste",
    completed : false
}

// creation objet,
let ToDosListe = {
    todos :[item1, item2,item3,item4],
    titre :"ma todo",

// ajout fonction affichage des todos + 
    displayTodos: function () {
        console.log (this.titre, " : ");
        for (let i=0; i < this.todos.length; i++){

// afficher etat des todos 
            if (this.todos[i].completed) {
            console.log("(X)" ,this.todos[i].texte);
            } else {
            console.log("( )" ,this.todos[i].texte);

            }
        }
    },
    
    // fonction ajout de todo 
    addTodo: function (newItem){ 
        
        this.todos.push (
                {
                    texte: newItem, 
                    completed: false 
                }
            );
        this.displayTodos();
    },

// fonction suppression de todo    
    deleteTodo: function (index){
        this.todos.splice(index, 1);
        this.displayTodos();
    },

// fonction modification todo
    changeTodo: function (index, newtodo){
        item = {
            texte: newtodo,
            completed: this.todos[index].completed
        }
    this.todos[index] = item;    
    this.displayTodos();
    },

// change etat todo
    changeEtat: function (index){
        let todo = this.todos[index]
        todo.completed =!todo.completed
        this.displayTodos();

    },
// change etat general
    changeTotal: function() {
        let totalTodos = this.todos.length;
        let completedTodos = 0;

// calculer le nombre de vrai 
        for (let i=0; i < totalTodos; i++) {
            if (this.todos[i].completed) {
                completedTodos++;
            }
        }
        
// si le nombre total de vrai = nombre items = faux sinon vrai
        if (totalTodos===completedTodos){
            for(let i=0; i<totalTodos; i++){
                this.todos[i].completed = false;
            }
        } else {
            for(let i=0; i<totalTodos; i++){
                this.todos[i].completed = true;
            }
        }
        ToDosListe.displayTodos()
    }
}
// creation objet pour gerer les evenement du html 
let handler = {
    displayTodos: function(){
        ToDosListe.displayTodos();
    },
    changeTotal: function(){
        ToDosListe.changeTotal();
        view.displayTodos();

    },
    addTodo:function(){
        let addTodoTextInput = document.getElementById("addTodoTextInput");
        ToDosListe.addTodo(addTodoTextInput.value);
        addTodoTextInput.value= '';
        view.displayTodos();
    },
    deleteTodo:function(){
        let indexNumberinput = document.getElementById("indexNumberInput");
        ToDosListe.deleteTodo(indexNumberinput.value);
        view.displayTodos();

    },
    changeTodo:function(){
        let changeTodoTextInput2 = document.getElementById("changeTodoTextInput2");
        let indexNumberInput2 = document.getElementById("indexNumberInput2");
        ToDosListe.changeTodo(indexNumberInput2.value,changeTodoTextInput2.value);
        changeTodoTextInput2.value= '';
        indexNumberInput2.value= '';
        view.displayTodos();

    },
    changeEtat:function(){
        let changeEtatIndexInput = document.getElementById('changeEtatIndexInput');
        ToDosListe.changeEtat(changeEtatIndexInput.value);
        view.displayTodos();
    },
    validTextArea:function (texteArea){
        if (event.key==="Enter"){
        event.preventDefault();
        this.addTodo(texteArea.value);
        texteArea.value = "";
        }
    }
    
}

// creation objet qui gere l'interface graphique
let view = {
    displayTodos:function(){
        let todoUl = document.querySelector('ul');
        todoUl.innerHTML= '';
// pour chaque element du tableau ajouter li à Lu 
        for (let i=0; i< ToDosListe.todos.length; i++){
            let todo = ToDosListe.todos[i]; 
            let todoLi = document.createElement('li');
// creer check box etat des li
//             let todoTextCompleted = '';
//             if (todo.completed){
//                 todoTextCompleted = '✔' + todo.texte;
//             } else {
//                 todoTextCompleted = '' + todo.texte;
//             }

//injecter le texte de add a li
            todoLi.textContent =todo.texte;
            if(todo.completed){
                todoLi.classList.add("fait");
            }
            
            todoUl.appendChild(todoLi);
        }
    }
}
view.displayTodos();
ToDosListe.displayTodos();
//ToDosListe.addTodo(item4);
//ToDosListe.deleteTodo(2);
