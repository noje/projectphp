<?php 

    session_start();

    $logged = false;
    if(isset($_GET['logout'])){ 
        session_destroy();
        session_start();
    }else{
        if(isset($_SESSION['logged_in']) and $_SESSION and $_SESSION['logged_in'] == true) {
            $logged = true;
        }
    }
    
    

    if( !isset($_GET['page'])){
        $current_page="contaccueil";
    }else{
        $current_page=$_GET["page"]; 
    } 
    
    if(empty($current_page)){
        $current_page = "contaccueil";
    }
    
    
    include 'header.php';
?>

    <div class="screenpapa">
        <img class="screenlogo" src="images/screenmac.png">

        <?php include 'projetphp/'.$current_page.'.php';
            ?>           
    </div>
    

<?php
    include 'footer.php';
?>





