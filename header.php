<!doctype html>

<html lang="FR">
<head>   
    <meta charset="utf-8">
    <link rel="shortcut icon" type="image/mac.png" href="images/mac.png">
    <link rel="stylesheet" href="style.css">
    <title>ProjetPhp</title>
</head>

    <body>
        <header>
            <div class ="barlogmenu">
                <div class="logo">
                    <img class="logoimg"
                        src="images/mac.png" 
                        height="65,3px" 
                        width="60,3px" 
                    />
                </div>

                <div class="menu">
                        
                    <span class="buton">
                        <a class="<?php if ($current_page =="contaccueil") echo "activePage"; ?> " href="?page=contaccueil"> /~Accueil</a>
                    </span>
                    <span class="buton">
                        <a class="<?php if ($current_page =="contweb") echo "activePage"; ?> "  href="?page=contweb">/~Web</a>
                    </span>
                    <span class="buton">
                        <a class="<?php if ($current_page =="contlinux") echo "activePage"; ?> "  href="?page=contlinux">/~Linux</a>
                    </span>
                    <span class="buton">
                        <a class="<?php if ($current_page =="contoutils") echo "activePage"; ?> "  href="?page=contoutils">/~Outils</a>
                    </span>
                    <span class="buton">
                        <a class="<?php if ($current_page =="contwordpress") echo "activePage"; ?> " href="?page=contwordpress">/~Wordpress</a>
                    </span>
                    <span class="buton">
                        <a class="<?php if ($current_page =="contjs") echo "activePage"; ?> " href="?page=contjs">/~javaScript</a>
                    </span>
                    <span class="buton">
                        <a class="<?php if ($current_page =="contmenu") echo "activePage"; ?> " href="?page=contmenu">/~menu</a>
                    </span>
                    <span class="buton">
                        <a class="<?php if ($current_page =="contconnect") echo "activePage"; ?> " <?php if($logged) {
                            echo 'href="?page=contconnect&logout" >/~Disconnect'; 
                        }else{
                            echo 'href="?page=contconnect">/~Connect'; 
                        }
                        
                        ?> 
                        </a>
                    </span>

                </div>
            </div>
        </header>

